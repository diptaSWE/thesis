import math
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
matplotlib.style.use('ggplot')


def Summation(object):
    total = 0
    for value in object.iloc[:, 0]:
        total += float(value)

    return total


def SquareSummation(object):
    total = 0
    for value in object.iloc[:, 0]:
        total += float(value)*float(value)

    return total


def TwoValueMultiplySummation(object1, object2):
    total = 0
    for index in range(0, len(object1) - 1):
        total += float(object1.iloc[index, 0]) * float(object2.iloc[index, 0])

    return total


def Corelation(X, Y):
    X_Sum = Summation(X)
    Y_Sum = Summation(Y)
    XY_Sum = TwoValueMultiplySummation(X, Y)
    N = len(X)
    X_sqr_Sum = SquareSummation(X)
    Y_sqr_Sum = SquareSummation(Y)

    Co_Relation = 0

    try:
        Co_Relation = (XY_Sum - (X_Sum*Y_Sum)/N) / math.sqrt((X_sqr_Sum -
                                                              math.pow(X_Sum, 2)/N)*(Y_sqr_Sum - math.pow(Y_Sum, 2)/N))
        pass
    except ZeroDivisionError:
        Co_Relation = 0

    return Co_Relation


def Calculate_Corelation(df, target_key, correlation_df):
    for key in df.keys()[:-1]:
        _corelation = Corelation(df[[key]], df[[target_key]])
        if(_corelation > 0):
            print('The relation between {0} & {1} is positive, (corelation) = {2}'.format(
                key, target_key,_corelation))
            correlation_df = append_Correlation_value(correlation_df,key,str(_corelation),'Positive')
        elif(_corelation < 0):
            print('The relation between {0} & {1} is negative, (corelation) = {2}'.format(
                key, target_key,_corelation))
            correlation_df = append_Correlation_value(correlation_df,key,str(_corelation),'Negative')
        else:
            print('There is no relation between {0} & {1}'.format(
                key, target_key))
            correlation_df = append_Correlation_value(correlation_df,key,str(_corelation),'No Relation')
    
    return correlation_df

def create_Correlation_dataframe():
    col_names =  ['Attribute', 'Correlation', 'value','Result']
    cprrelation_df  = pd.DataFrame(columns = col_names)

    return cprrelation_df

def append_Correlation_value(df,col_name,correlation, value):
    df.loc[len(df)] = [col_name,col_name,correlation, value]

    return df

def save_Correlation_table(correlation_df, fileName):
    correlation_df.to_csv(fileName,sep = ',', encoding = 'utf-8')
    print('Done')


df = pd.read_csv('data/dataset.csv', encoding='windows-1252')
# df = pd.read_csv('data/test.csv')
df.head()
target = 'CGPA'

correlation_df = create_Correlation_dataframe()


fileName = 'data/correlation_result.csv'

# target = 'species'
# print(df.iloc[:,0:42].corr(df.iloc[:,-1]))
correlation_df = Calculate_Corelation(df, target, correlation_df)

save_Correlation_table(correlation_df, fileName)
