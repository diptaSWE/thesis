# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 15:46:35 2018

@author: DiptaDas
"""

#Importing The Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

#Variable
data2 = 0;

#Importing the dataset
dataset = pd.read_csv('Data_ED.csv',encoding='cp1252')
X = dataset.iloc[:, :-1].values
Y = dataset.iloc[:, 78].values
y = Y.tolist()

#Taking care of missing data
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values= 'NaN', axis = 0)
imputer = imputer.fit(X[:, 0:77])
X[:, 0:77] = imputer.transform(X[:, 0:77])

print (Y[[245][0]])
#ED
final_result = [];


for a in range(77):
    data2 = 0;
    for b in range(679):
        data1 = X[b, a] - Y[b]
        data2 = data2 + data1
    data = math.sqrt(abs(data2))
    final_result.append(data)