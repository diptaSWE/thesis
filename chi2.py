# -*- coding: utf-8 -*-
"""
Created on Wed Aug 29 08:40:54 2018

@author: DiptaDas
"""
#Importing The Libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#Importing the dataset
dataset = pd.read_csv('Data1.csv',encoding='cp1252')
X = dataset.iloc[:, :-1].values
Y = dataset.iloc[:, 78].values
y = Y.tolist()

#Taking care of missing data
from sklearn.preprocessing import Imputer
imputer = Imputer(missing_values= 'NaN', axis = 0)
imputer = imputer.fit(X[:, 0:77])
X[:, 0:77] = imputer.transform(X[:, 0:77])

#Implementing Chi square
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2
X_new = SelectKBest(chi2, k=10).fit_transform(X,y)
